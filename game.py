from player import Player


class Game:
    def __init__(self, logger):
        self.reset()
        self._player1 = Player(1)
        self._player2 = Player(2)
        self._logger = logger

    def reset(self):
        self._board = [None] * 9
        self._steps_counter = 0
        self._is_player1_turn = True
        self._is_game_finished = False

    cell_value = {
        False: 'O',
        True: 'X',
        None: '*'
    }

    def draw_board(self):
        for i in range(0, 9, 3):
            print(' | '.join(
                Game.cell_value[cell] for cell in self._board[i:i+3]
            ))

    def get_index(self):
        current_player_name = self._player1.get_name(
        ) if self._is_player1_turn else self._player2.get_name()
        index = 0
        while index == 0:
            try:
                input_index = int(
                    input(f'{current_player_name} make your move: '))
                if input_index in range(1, 10) and self._board[
                    input_index - 1
                ] is None:
                    index = input_index
                else:
                    print('Value should be in range 1-9 and choosen cell ' +
                          'should not be filled yet')
            except ValueError:
                print('Value should be a number in range 1-9')
        return index - 1

    def make_move(self, index):
        self._board[index] = self._is_player1_turn
        self._steps_counter += 1

    def check_game_finished(self, index):
        column = index % 3
        row_start = index - column
        is_even = index % 2 == 0
        self._is_game_finished = (
            self._steps_counter == 9 or
            self.is_line_filled(column, 9, 3) or
            self.is_line_filled(row_start, row_start + 3, 1) or
            is_even and self.is_line_filled(0, 9, 4) or
            is_even and self.is_line_filled(2, 7, 2)
        )

    def is_line_filled(self, start, end, step):
        return all(self._board[i] == self._is_player1_turn for i in range(
            start, end, step))

    def replay(self):
        play_again = input('Play again? [Y\\n]: ')
        if play_again.lower() == 'y' or play_again == '':
            self._player1, self._player2 = self._player2, self._player1
            self.reset()
            self.play_game()

    def finish_game(self):
        is_draw = self._steps_counter == 9
        winner = self._player1 if not self._is_player1_turn else self._player2
        if not is_draw:
            winner.score_increment()
        score_display = f'{self._player1.get_name()} {self._player1.get_score()} - {self._player2.get_score()} {self._player2.get_name()}'
        print('Game over in a draw') if is_draw else print(
            f'Game over. {winner.get_name()} won!!!')
        print('Score:\n' + score_display)
        self.log_score(is_draw, winner, score_display)

    def log_score(self, is_draw, winner, score_display):
        if is_draw:
            self._logger.log_record('Game over in a draw\n' + score_display)
        else:
            self._logger.log_record(
                f'{winner.get_name()} won.\n{score_display}')

    def play_game(self):
        self.draw_board()
        while not self._is_game_finished:
            index = self.get_index()
            self.make_move(index)
            self.check_game_finished(index)
            self.draw_board()
            self._is_player1_turn = not self._is_player1_turn
        self.finish_game()
        self.replay()
