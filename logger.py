import os
import logging


class Logger:
    file_name = 'score.txt'

    def __init__(self):
        if not os.path.exists(Logger.file_name):
            with open(Logger.file_name, 'w'):
                pass

        logging.basicConfig(
            filename=Logger.file_name,
            format='%(asctime)s - %(message)s',
            datefmt='%d-%b-%y %H:%M',
            level=logging.INFO
        )

    def show_log(self):
        try:
            with open(Logger.file_name, 'r') as f:
                f.seek(0)
                if os.stat(Logger.file_name).st_size == 0:
                    raise FileNotFoundError
                print('\n')
                print(f.read())
        except FileNotFoundError:
            print('There are no records yet')

    def clear_log(self):
        with open(Logger.file_name, 'w'):
            print('The log was succefully cleared!')

    def log_record(self, message):
        logging.info(message)
