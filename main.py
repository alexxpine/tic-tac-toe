from menu import Menu


def main():
    menu = Menu()
    while True:
        item = menu.show_menu_items()
        menu.choose_menu_item(item)


if __name__ == "__main__":
    main()
