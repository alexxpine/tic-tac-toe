from game import Game
from logger import Logger
import sys


class Menu:
    def __init__(self):
        self._logger = Logger()

    def start_game(self):
        self._game = Game(self._logger)
        self._game.play_game()

    def show_log(self):
        self._logger.show_log()

    def clear_log(self):
        self._logger.clear_log()

    def exit(self):
        sys.exit()

    def show_menu_items(self):
        print('')
        print('Main Menu:')
        print('1. Start new game')
        print('2. Show logs')
        print('3. Clear logs')
        print('4. Exit')
        print('')

        reply = 0
        while reply not in range(1, 5):
            try:
                reply = int(input('Please, choose one option: '))
                if reply < 1 or reply > 4:
                    raise ValueError
            except ValueError:
                print('Choose between given options (1-4)\n')
        return reply

    def choose_menu_item(self, item):
        if item == 1:
            self.start_game()
        elif item == 2:
            self.show_log()
        elif item == 3:
            self.clear_log()
        elif item == 4:
            self.exit()
