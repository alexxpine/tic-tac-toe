class Player:
    def __init__(self, order):
        self._name = input(f'Enter {order} player name: ')
        self._score = 0

    def get_name(self):
        return self._name

    def get_score(self):
        return self._score

    def score_increment(self):
        self._score += 1
